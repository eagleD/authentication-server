import { IsEmail, IsOptional } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class SearchUserDto {
  @Field({ nullable: true })
  @IsOptional()
  readonly id?: number;

  @Field({ nullable: true })
  @IsOptional()
  readonly username?: string;

  @Field({ nullable: true })
  @IsOptional()
  @IsEmail()
  readonly email?: string;

  @Field({ nullable: true })
  @IsOptional()
  @IsEmail()
  readonly isActive?: boolean;

  @Field({ nullable: true })
  @IsOptional()
  password?: string;
}
