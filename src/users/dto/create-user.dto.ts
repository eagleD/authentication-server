import { IsNotEmpty, IsOptional } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class CreateUserDto {
  @Field()
  @IsNotEmpty()
  readonly username: string;

  @Field()
  @IsNotEmpty()
  readonly email: string;

  @Field()
  @IsNotEmpty()
  readonly password: string;

  @Field({ nullable: true })
  @IsOptional()
  readonly type?: string;

  @Field({ nullable: true })
  @IsOptional()
  readonly isSuperAdmin?: boolean;

  @Field({ nullable: true })
  @IsOptional()
  readonly isActive?: boolean;
}
