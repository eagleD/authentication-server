import { Field, ID, ObjectType } from 'type-graphql';

@ObjectType()
export class User {
  @Field(type => ID)
  id: number;

  @Field()
  username: string;

  @Field()
  email: string;

  @Field({ nullable: true })
  isActive?: boolean;

  @Field({ nullable: true })
  isSuperAdmin?: boolean;

  @Field({ nullable: true })
  type?: string;

  @Field({ nullable: true })
  createdAt?: Date;

  @Field({ nullable: true })
  lastLogin?: Date;
}
