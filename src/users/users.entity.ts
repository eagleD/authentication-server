import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeInsert,
  CreateDateColumn,
  OneToOne,
} from 'typeorm';
import {
  IsEmail,
  IsNotEmpty,
  IsBoolean,
  IsDate,
  IsEnum,
  IsOptional,
} from 'class-validator';
import * as crypto from 'crypto';
import { RefreshTokensEntity } from '../auth/entities/refresh-tokens.entity';
export enum UserType {
  INTERNAL = 'INTERNAL',
  EXTERNAL = 'EXTERNAL',
}

@Entity('users')
export class UsersEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  @IsNotEmpty()
  username: string;

  @Column({ unique: true })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Column()
  password: string;

  @BeforeInsert()
  hashPassword() {
    this.password = crypto.createHmac('sha256', this.password).digest('hex');
  }

  @Column({ default: false })
  @IsBoolean()
  @IsOptional()
  isActive: boolean;

  @Column({ default: false })
  @IsBoolean()
  @IsOptional()
  isSuperAdmin: boolean;

  @Column({
    type: 'enum',
    enum: UserType,
    default: UserType.EXTERNAL,
  })
  @IsEnum(UserType)
  @IsOptional()
  type: UserType;

  @Column({ type: 'datetime', nullable: true })
  @IsDate()
  @IsOptional()
  lastLogin: Date;

  @CreateDateColumn()
  createdAt: Date;

  @Column({ type: 'datetime', default: null })
  deletedAt: Date;

  @OneToOne(type => RefreshTokensEntity, refreshToken => refreshToken.user, {
    cascade: true,
  })
  refreshToken: RefreshTokensEntity;
}
