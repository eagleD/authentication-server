import {
  Injectable,
  BadRequestException,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository } from 'typeorm';
import * as crypto from 'crypto';
import { validate } from 'class-validator';
import * as dateFormat from 'dateformat';

import { UsersEntity, UserType } from './users.entity';
import { CreateUserDto, SearchUserDto } from './dto';
import { User } from './users.model';
import { ERROR_MSG } from '../common/constants/messages';
import { DEFAULT_USER_TYPE } from '../common/constants/users';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UsersEntity)
    private readonly userRepository: Repository<UsersEntity>,
  ) {}

  async findAll(): Promise<UsersEntity[]> {
    return await this.userRepository.find();
  }

  async findOneByUsername(username: string): Promise<UsersEntity> {
    return await this.userRepository.findOne({ username });
  }

  async findOneByOptions(
    search: SearchUserDto,
    relations: string[] = [],
  ): Promise<UsersEntity> {
    const where: any = {};
    if (search.id) where.id = search.id;
    if (search.email) where.email = search.email;
    if (search.username) where.username = search.username;
    if (search.isActive) where.isActive = search.isActive;
    if (search.password) {
      where.password = crypto
        .createHmac('sha256', search.password)
        .digest('hex');
    }

    const options: any = { where: where };
    if (relations.length > 0) options.relations = relations;
    return await this.userRepository.findOne(options);
  }

  async create(dto: CreateUserDto): Promise<User> {
    const { username, email, password, type } = dto;

    // check uniqueness of username/email
    const qb = await getRepository(UsersEntity)
      .createQueryBuilder('user')
      .where('user.username = :username', { username })
      .orWhere('user.email = :email', { email });

    const user = await qb.getOne();
    if (user) {
      throw new BadRequestException(ERROR_MSG.USERNAME_EMAIL_UNIQUE);
    }

    const userType =
      type && type.toUpperCase() == UserType.INTERNAL
        ? UserType.INTERNAL
        : DEFAULT_USER_TYPE;

    // create new user
    const userDta = {
      username: username,
      email: email,
      password: password,
      isSuperAdmin: dto.isSuperAdmin ? dto.isSuperAdmin : false,
      isActive: dto.isActive ? dto.isActive : false,
      type: userType,
    };

    let newUser = await this.userRepository.create(userDta);

    const errors = await validate(newUser);
    if (errors.length > 0) {
      throw new BadRequestException(ERROR_MSG.VALIDATION_FAILED);
    } else {
      const savedUser = await this.userRepository.save(newUser);
      return this.buildUserRO(savedUser);
    }
  }

  async updateLastLogin(userId: number): Promise<boolean> {
    const user = await this.userRepository.findOne({ id: userId });
    if (!user) {
      throw new NotFoundException(ERROR_MSG.USER_NOT_FOUND);
    }

    const updated = await this.userRepository.update(
      { id: userId },
      { lastLogin: dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss') },
    );

    return !!updated;
  }

  async activate(userId: number): Promise<User> {
    let user = await this.userRepository.findOne({ id: userId });
    if (!user) {
      throw new NotFoundException(ERROR_MSG.USER_NOT_FOUND);
    }

    await this.userRepository.update({ id: userId }, { isActive: true });

    //requery user
    user = await this.userRepository.findOne({ id: userId });
    return this.buildUserRO(user);
  }

  async setNewPassword(user: User, newPassword: string): Promise<boolean> {
    if (!user || !user.isActive) {
      throw new NotFoundException(ERROR_MSG.USER_NOT_FOUND);
    }
    //update
    await this.userRepository.update(
      { id: user.id },
      { password: crypto.createHmac('sha256', newPassword).digest('hex') },
    );
    //requery user
    user = await this.userRepository.findOne({ id: user.id });
    if (!user) {
      throw new InternalServerErrorException(ERROR_MSG.INTERNAL_ERROR);
    }
    return true;
  }

  private buildUserRO(user: UsersEntity) {
    const userRO: any = {
      id: user.id,
      username: user.username,
      email: user.email,
      isActive: user.isActive,
      isSuperAdmin: user.isSuperAdmin,
      type: user.type,
    };
    return userRO;
  }
}
