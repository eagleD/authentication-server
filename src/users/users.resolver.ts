import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { UseGuards, NotFoundException } from '@nestjs/common';

import { User } from './users.model';
import { UsersService } from './users.service';
import { ERROR_MSG } from '../common/constants/messages';
import { GraphqlAuthGuard } from '../common/guards/graphql-auth.guard';
import { User as CurrentUser } from './users.decorator';

@Resolver(of => User)
@UseGuards(GraphqlAuthGuard)
export class UsersResolver {
  constructor(private readonly userService: UsersService) {}

  @Query(returns => [User])
  async getUsers(): Promise<User[]> {
    return await this.userService.findAll();
  }

  @Query(returns => User)
  async getUser(@Args('username') username: string): Promise<User> {
    const user = await this.userService.findOneByUsername(username);
    if (!user) {
      throw new NotFoundException(ERROR_MSG.USER_NOT_FOUND);
    }
    return user;
  }

  @Query(returns => User)
  async whoami(@CurrentUser() user: User): Promise<User> {
    const currentUser = await this.userService.findOneByOptions({
      id: user.id,
    });
    if (!currentUser) {
      throw new NotFoundException(ERROR_MSG.USER_NOT_FOUND);
    }
    return currentUser;
  }
}
