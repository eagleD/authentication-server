import { Injectable } from '@nestjs/common';
import { Cron, Interval, Timeout, NestSchedule } from 'nest-schedule';
const fs = require('fs');

@Injectable() // Only support SINGLETON scope
export class ScheduleService extends NestSchedule {
  // running every 30 minutes
  @Cron('*/30 * * * *', {
    startTime: new Date(),
    endTime: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
  })
  async cleanBlackList() {
    const blacklistFile = process.env.BLACKLISTED_TOKENS_FILE;
    if (fs.existsSync(blacklistFile)) {
      const newList = [];
      const bList = JSON.parse(fs.readFileSync(blacklistFile, 'utf8'));
      for (let obj of bList) {
        if (Date.now() < obj.exp * 1000) {
          //token is still valid
          newList.push(obj);
        }
      }
      fs.writeFileSync(blacklistFile, JSON.stringify(newList));
    }
  }

  @Timeout(5000)
  onceJob() {
    // console.log('executing once job');
  }

  @Interval(2000)
  intervalJob() {
    // console.log('executing interval job');

    // if you want to cancel the job, you should return true;
    return true;
  }
}
