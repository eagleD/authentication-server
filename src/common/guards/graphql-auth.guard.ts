import {
  Injectable,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Observable } from 'rxjs';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';
import * as jwt from 'jsonwebtoken';
import { ERROR_MSG } from '../constants/messages';
const fs = require('fs');

@Injectable()
export class GraphqlAuthGuard extends AuthGuard('jwt') {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();
    if (req.headers.authorization) {
      ctx.getContext().user = this.validateJWTtoken(req.headers.authorization);
    }

    return super.canActivate(new ExecutionContextHost([req]));
  }

  handleRequest(err, user, info) {
    if (err || !user) {
      throw err || new UnauthorizedException(ERROR_MSG.UNAUTHORIZED_ACCESS);
    }
    return user;
  }

  private validateJWTtoken(auth: string) {
    if (auth.split(' ')[0] !== 'Bearer') {
      throw new UnauthorizedException(ERROR_MSG.INVALID_ACCESS_TOKEN);
    }
    const token = auth.split(' ')[1];

    try {
      const decoded: any = jwt.verify(token, process.env.ACCESS_SECRET_KEY);
      const blacklist = process.env.BLACKLISTED_TOKENS_FILE;
      if (!fs.existsSync(blacklist)) {
        fs.writeFileSync(blacklist, '[]');
      }
      var blacklistedTokens = JSON.parse(fs.readFileSync(blacklist, 'utf8'));
      const found = blacklistedTokens.some(el => el.token === token);
      if (found) {
        throw Error(ERROR_MSG.UNAUTHORIZED_ACCESS);
      }
      return decoded;
    } catch (err) {
      const message = 'Token error: ' + (err.message || err.name);
      throw new UnauthorizedException(message);
    }
  }
}
