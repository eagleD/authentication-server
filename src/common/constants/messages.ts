export const ERROR_MSG = {
  UNAUTHORIZED_ACCESS: 'Unauthorized Access.',
  USER_NOT_ACTIVE: 'User is not active.',
  USER_ACTIVE: 'User is already active.',
  INVALID_LOGIN: 'Invalid username or password.',
  INVALID_VERIFICATION_CODE: 'Verification code is not valid.',
  USER_NOT_FOUND: 'User not found.',
  INTERNAL_ERROR: 'Internal server error.',
  INVALID_REQUEST: 'Invalid request.',
  RESET_PASSWORD_EXPIRED: 'Reset password link is expired.',
  VALIDATION_FAILED: 'Input data validation failed.',
  USERNAME_EMAIL_UNIQUE: 'Username and email must be unique.',
  INVALID_ACCESS_TOKEN: 'Invalid token.',
  INVALID_REFRESH_TOKEN: 'Invalid token.',
  AUTHENTICATION_ERROR: 'Authentication error.',
  SYS_NODE_EXISTS: 'System node name already exists.',
  SYS_NODE_NOT_FOUND: 'System node not found.',
};

export const SUCCESS_MSG = {};
