import { UserType } from '../../users/users.entity';

export const DEFAULT_USER_TYPE = UserType.EXTERNAL;
export const RESET_PASSWORD_EXPIRY_MINS = 60; //60 in minutes;
