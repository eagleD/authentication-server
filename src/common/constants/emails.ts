export const EMAIL_CONSTS = {
  NOREPLY_EMAIL: 'noreply@astig.it',
  NOREPLY_NAME: 'noreply',

  EMAIL_VERIFICATION_TITLE: 'AstigIT: Verify your account',
  RESET_PASSWORD_TITLE: 'AstigIT: Reset your password',
};
