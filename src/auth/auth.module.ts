import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

import { AuthService } from './auth.service';
import { TokenService } from './token.service';
import { AuthResolver } from './auth.resolver';
import { UsersModule } from '../users/users.module';
import { JwtStrategy } from './jwt.strategy';
import { RefreshTokensEntity } from './entities/refresh-tokens.entity';
import { EmailVerificationsEntity } from './entities/email-verifications.entity';
import { ForgotPasswordsEntity } from './entities/forgot-passwords.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      RefreshTokensEntity,
      EmailVerificationsEntity,
      ForgotPasswordsEntity,
    ]),
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.SECRET_KEY,
      signOptions: { expiresIn: process.env.ACCESS_TOKEN_EXPIRY },
    }),
  ],
  providers: [AuthService, TokenService, AuthResolver, JwtStrategy],
})
export class AuthModule {}
