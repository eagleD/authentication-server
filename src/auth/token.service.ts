import { Repository } from 'typeorm';
import * as jwt from 'jsonwebtoken';
import { v4 as uuid } from 'uuid';
const moment = require('moment');
const fs = require('fs');

import { UsersEntity } from '../users/users.entity';
import { InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RefreshTokensEntity } from './entities/refresh-tokens.entity';
import { EmailVerificationsEntity } from './entities/email-verifications.entity';
import { ForgotPasswordsEntity } from './entities/forgot-passwords.entity';

export class TokenService {
  constructor(
    @InjectRepository(RefreshTokensEntity)
    private readonly refreshTokenRepository: Repository<RefreshTokensEntity>,
    @InjectRepository(EmailVerificationsEntity)
    private readonly emailVerificationRepository: Repository<
      EmailVerificationsEntity
    >,
    @InjectRepository(ForgotPasswordsEntity)
    private readonly forgotPasswordRepository: Repository<
      ForgotPasswordsEntity
    >,
  ) {}

  async createAccessToken(user: UsersEntity): Promise<string> {
    const token = jwt.sign(
      {
        id: user.id,
        email: user.email,
        username: user.username,
      },
      process.env.ACCESS_SECRET_KEY,
      { expiresIn: process.env.ACCESS_TOKEN_EXPIRY },
    );

    if (!token) {
      throw new InternalServerErrorException();
    }
    return token;
  }

  async createRefreshToken(user: UsersEntity): Promise<string> {
    const token = uuid();
    const expiresAt = moment()
      .add(process.env.REFRESH_TOKEN_EXPIRY, 'm')
      .format('YYYY-MM-DD HH:mm:ss', process.env.REFRESH_TOKEN_EXPIRY);

    let rToken = await this.refreshTokenRepository.findOne({
      where: { user },
    });

    const data = { token, expiresAt, user };

    if (rToken) {
      //update
      await this.refreshTokenRepository.update({ id: rToken.id }, data);
    } else {
      //create a new one
      //add to db
      rToken = await this.refreshTokenRepository.create(data);
      await this.refreshTokenRepository.save(rToken);
    }

    return token;
  }

  async removeRefreshToken(token: string): Promise<boolean> {
    const rToken = await this.refreshTokenRepository.findOne({ token });
    if (rToken) {
      await this.refreshTokenRepository.delete(rToken);
    }
    return true;
  }

  async blackListAccessToken(
    access_token: string,
    expiration: number,
  ): Promise<boolean> {
    const blacklistedTokens = await this.getBlacklistedTokens();
    const found = blacklistedTokens.some(el => el.token === access_token);
    if (!found) {
      const newToken = { token: access_token, exp: expiration };
      blacklistedTokens.push(newToken);
      const data = JSON.stringify(blacklistedTokens);
      fs.writeFileSync(process.env.BLACKLISTED_TOKENS_FILE, data);

      return true;
    }
  }

  async getBlacklistedTokens(): Promise<any> {
    const blacklistFile = process.env.BLACKLISTED_TOKENS_FILE;
    if (!fs.existsSync(blacklistFile)) {
      fs.writeFileSync(blacklistFile, '[]');
    }

    return JSON.parse(fs.readFileSync(blacklistFile, 'utf8'));
  }

  async createEmailVerificationToken(
    email: string,
  ): Promise<EmailVerificationsEntity> {
    let emailVerification = await this.emailVerificationRepository.findOne({
      email,
    });

    const data = {
      email: email,
      token: uuid(),
      timestamp: new Date(),
    };

    if (!emailVerification) {
      // add new email verification
      const newVerif = await this.emailVerificationRepository.create(data);
      emailVerification = await this.emailVerificationRepository.save(newVerif);
    } else {
      //update existing one
      await this.emailVerificationRepository.update(
        { id: emailVerification.id },
        data,
      );
      emailVerification = await this.emailVerificationRepository.findOne({
        id: emailVerification.id,
      });
    }
    return emailVerification;
  }

  async findEmailVerificationToken(
    token: string,
  ): Promise<EmailVerificationsEntity> {
    return await this.emailVerificationRepository.findOne({ token });
  }

  async deleteEmailVerificationToken(token: string): Promise<boolean> {
    const vToken = await this.emailVerificationRepository.findOne({ token });
    if (vToken) await this.emailVerificationRepository.delete(vToken);

    return true;
  }

  async createForgotPasswordToken(
    email: string,
  ): Promise<ForgotPasswordsEntity> {
    let forgotPassword = await this.forgotPasswordRepository.findOne({ email });
    //prepare data
    const data = {
      email: email,
      token: uuid(),
    };
    if (!forgotPassword) {
      const newFP = await this.forgotPasswordRepository.create(data);
      forgotPassword = await this.forgotPasswordRepository.save(newFP);
    } else {
      await this.forgotPasswordRepository.update(
        { id: forgotPassword.id },
        data,
      );
      //requery to get the new data
      forgotPassword = await this.forgotPasswordRepository.findOne({
        id: forgotPassword.id,
      });
    }

    return forgotPassword;
  }

  async findForgotPasswordToken(
    email: string,
    token: string,
  ): Promise<ForgotPasswordsEntity> {
    return await this.forgotPasswordRepository.findOne({ email, token });
  }

  async deleteForgotPasswordToken(token: string): Promise<boolean> {
    const fpToken = await this.forgotPasswordRepository.findOne({ token });
    if (fpToken) await this.forgotPasswordRepository.delete({ id: fpToken.id });

    return true;
  }
}
