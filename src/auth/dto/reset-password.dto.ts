import { InputType, Field } from 'type-graphql';
import { IsEmail, IsString, IsNotEmpty } from 'class-validator';

@InputType()
export class ResetPasswordDto {
  @Field()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @Field()
  @IsString()
  @IsNotEmpty()
  readonly newPassword: string;

  @Field()
  @IsString()
  @IsNotEmpty()
  readonly token: string;

  @Field()
  @IsString()
  @IsNotEmpty()
  readonly currentPassword: string;
}
