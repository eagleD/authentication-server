import { IsNotEmpty } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class LoginDto {
  @Field()
  @IsNotEmpty()
  readonly username: string;

  @Field()
  @IsNotEmpty()
  readonly password: string;

  @Field()
  readonly isActive: boolean = true;
}
