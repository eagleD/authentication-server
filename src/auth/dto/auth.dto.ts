import { ObjectType, Field, InputType } from 'type-graphql';
import { IsNotEmpty } from 'class-validator';

@ObjectType()
export class AuthRO {
  @Field()
  readonly access_token: string;

  @Field()
  readonly refresh_token?: string;

  @Field()
  readonly expires_in?: number;

  @Field()
  readonly status: number;
}

@InputType()
export class AuthTokenDto {
  @Field()
  @IsNotEmpty()
  readonly access_token: string;

  @Field()
  @IsNotEmpty()
  readonly refresh_token?: string;
}
