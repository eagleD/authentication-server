export interface JwtPayload {
  username: string;
  iat?: number;
  expiresIn?: string;
}
