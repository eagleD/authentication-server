import {
  Injectable,
  UnauthorizedException,
  NotFoundException,
  BadRequestException,
  InternalServerErrorException,
} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
const moment = require('moment');

import { LoginDto, AuthRO, AuthTokenDto, ResetPasswordDto } from './dto';
import { UsersService } from '../users/users.service';
import { ERROR_MSG } from '../common/constants/messages';
import { TokenService } from './token.service';
import { SearchUserDto, CreateUserDto } from '../users/dto';
import { User } from '../users/users.model';
import { MailerService } from '@nest-modules/mailer';
import { EMAIL_CONSTS } from '../common/constants/emails';
import { RESET_PASSWORD_EXPIRY_MINS } from '../common/constants/users';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly tokenService: TokenService,
    private readonly mailerService: MailerService,
  ) {}

  async register(data: CreateUserDto): Promise<User> {
    return await this.userService.create(data);
  }

  async login(loginDto: LoginDto): Promise<AuthRO> {
    const user = await this.userService.findOneByOptions(loginDto);
    if (!user) {
      throw new UnauthorizedException(ERROR_MSG.INVALID_LOGIN);
    }

    //update last login
    await this.userService.updateLastLogin(user.id);
    const access_token = await this.tokenService.createAccessToken(user);
    const refresh_token = await this.tokenService.createRefreshToken(user);

    return {
      access_token,
      refresh_token,
      expires_in: parseInt(process.env.ACCESS_TOKEN_EXPIRY_IN_SEC),
      status: 200,
    };
  }

  async logout(tokens: AuthTokenDto): Promise<boolean> {
    const { access_token, refresh_token } = tokens;
    //check if access token is not yet expired, blacklist it
    const decoded: any = jwt.decode(access_token);
    if (decoded && Date.now() < decoded.exp * 1000) {
      await this.tokenService.blackListAccessToken(access_token, decoded.exp);
    }
    ////remove refresh token
    return await this.tokenService.removeRefreshToken(refresh_token);
  }

  async refreshTokens(
    access_token: string,
    refresh_token: string,
  ): Promise<AuthRO> {
    //check if it its a valid user
    const decoded: any = jwt.decode(access_token);
    if (!decoded) {
      throw new BadRequestException(ERROR_MSG.INVALID_ACCESS_TOKEN);
    }
    const fOptions: SearchUserDto = { id: decoded.id, email: decoded.email };
    const user = await this.userService.findOneByOptions(fOptions, [
      'refreshToken',
    ]);
    if (!user) {
      throw new UnauthorizedException(
        ERROR_MSG.AUTHENTICATION_ERROR + ' ' + ERROR_MSG.USER_NOT_FOUND,
      );
    }
    //check if refresh token exist in db or if the same as the refresh_token passed
    const rToken = user.refreshToken;
    if (!rToken || rToken.token != refresh_token) {
      throw new UnauthorizedException(
        ERROR_MSG.AUTHENTICATION_ERROR + ' ' + ERROR_MSG.INVALID_REFRESH_TOKEN,
      );
    }
    //or if found, check if it is already expired
    if (moment(rToken.expiresAt).isSameOrBefore(moment())) {
      //token expired, remove token from db
      await this.tokenService.removeRefreshToken(rToken.token);
      throw new UnauthorizedException();
    } else {
      const new_aToken = await this.tokenService.createAccessToken(user);
      const new_rToken = await this.tokenService.createRefreshToken(user);

      return {
        access_token: new_aToken,
        refresh_token: new_rToken,
        expires_in: parseInt(process.env.ACCESS_TOKEN_EXPIRY_IN_SEC),
        status: 200,
      };
    }
  }

  async sendEmailVerification(email: string): Promise<boolean> {
    //check if email is registered
    const user = await this.userService.findOneByOptions({ email });
    if (!user) {
      throw new NotFoundException(ERROR_MSG.USER_NOT_FOUND);
    }
    if (user.isActive) {
      throw new BadRequestException(ERROR_MSG.USER_ACTIVE);
    }

    const emailVerification = await this.tokenService.createEmailVerificationToken(
      email,
    );
    if (emailVerification && emailVerification.token) {
      const sent = await this.mailerService
        .sendMail({
          to: user.email,
          subject: EMAIL_CONSTS.EMAIL_VERIFICATION_TITLE,
          template: 'email-verification',
          context: {
            // Data to be sent to template engine.
            registrationLink: emailVerification.token,
            username: user.username,
          },
        })
        .catch(err => {
          throw new InternalServerErrorException(err);
        });

      return !!sent;
    } else {
      throw new InternalServerErrorException(ERROR_MSG.INTERNAL_ERROR);
    }
  }

  async verifyEmailRegistration(token: string): Promise<Boolean> {
    const emailVerif = await this.tokenService.findEmailVerificationToken(
      token,
    );
    if (emailVerif && emailVerif.email) {
      const user = await this.userService.findOneByOptions({
        email: emailVerif.email,
      });
      if (user) {
        const activatedUser = await this.userService.activate(user.id);
        await this.tokenService.deleteEmailVerificationToken(token);

        return !!activatedUser;
      } else {
        throw new BadRequestException(ERROR_MSG.INVALID_REQUEST);
      }
    } else {
      throw new BadRequestException(ERROR_MSG.INVALID_VERIFICATION_CODE);
    }
  }

  async sendForgotPasswordEmail(email: string): Promise<boolean> {
    //check if email is registered
    const user = await this.userService.findOneByOptions({ email });
    if (!user || !user.isActive) {
      throw new NotFoundException(ERROR_MSG.USER_NOT_FOUND);
    }
    const newForgottenPassword = await this.tokenService.createForgotPasswordToken(
      email,
    );
    if (newForgottenPassword && newForgottenPassword.token) {
      const sent = await this.mailerService
        .sendMail({
          to: user.email,
          subject: EMAIL_CONSTS.RESET_PASSWORD_TITLE,
          template: 'reset-password',
          context: {
            // Data to be sent to template engine.
            link: newForgottenPassword.token,
            username: user.username,
            createdAt: newForgottenPassword.createdAt,
          },
        })
        .catch(err => {
          throw new InternalServerErrorException(err);
        });
      return !!sent;
    } else {
      throw new InternalServerErrorException(ERROR_MSG.INTERNAL_ERROR);
    }
  }

  async resetPassword(data: ResetPasswordDto): Promise<boolean> {
    const { email, currentPassword, newPassword, token } = data;
    const options: SearchUserDto = {
      email,
      password: currentPassword,
    };
    //validate user
    const user = await this.userService.findOneByOptions(options, [
      'refreshToken',
    ]);
    if (!user || !user.isActive) {
      throw new NotFoundException(ERROR_MSG.USER_NOT_FOUND);
    }

    //verify the token
    const forgotPassword = await this.tokenService.findForgotPasswordToken(
      email,
      token,
    );
    if (!forgotPassword) {
      throw new BadRequestException(ERROR_MSG.INVALID_REQUEST);
    }

    if (
      forgotPassword &&
      (new Date().getTime() - forgotPassword.createdAt.getTime()) / 60000 >
        RESET_PASSWORD_EXPIRY_MINS /*check if token was created in more than 60 minutes*/
    ) {
      throw new BadRequestException(ERROR_MSG.RESET_PASSWORD_EXPIRED);
    }

    //set the new password
    const isNewPassword = await this.userService.setNewPassword(
      user,
      newPassword,
    );
    if (isNewPassword) {
      //remove fogotten password token
      await this.tokenService.deleteForgotPasswordToken(token);
      //remove all refresh token to force logout
      if (user.refreshToken) {
        await this.tokenService.removeRefreshToken(user.refreshToken.token);
      }
      return true;
    } else {
      throw new InternalServerErrorException(ERROR_MSG.INTERNAL_ERROR);
    }
  }
}
