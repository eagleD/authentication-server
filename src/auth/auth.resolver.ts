import { Resolver, Mutation, Args } from '@nestjs/graphql';

import { AuthService } from './auth.service';
import { AuthRO, LoginDto, AuthTokenDto, ResetPasswordDto } from './dto';
import { CreateUserDto } from '../users/dto';

@Resolver()
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(returns => AuthRO)
  async login(@Args('data') data: LoginDto): Promise<AuthRO> {
    return await this.authService.login(data);
  }

  @Mutation(returns => Boolean)
  async logout(@Args('data') data: AuthTokenDto) {
    return await this.authService.logout(data);
  }

  @Mutation(returns => Boolean)
  async register(@Args('data') data: CreateUserDto): Promise<boolean> {
    //create the user
    const user = await this.authService.register(data);
    //only when the user is not yet active, send the email verification
    if (user && !user.isActive) {
      await this.authService.sendEmailVerification(user.email);
    }
    return !!user;
  }

  @Mutation(returns => AuthRO)
  async refreshTokens(@Args('tokens') tokens: AuthTokenDto): Promise<AuthRO> {
    return await this.authService.refreshTokens(
      tokens.access_token,
      tokens.refresh_token,
    );
  }

  @Mutation(returns => Boolean)
  async sendEmailVerfication(@Args('email') email: string): Promise<boolean> {
    return await this.authService.sendEmailVerification(email);
  }

  @Mutation(returns => Boolean)
  async verifyEmailRegistration(
    @Args('token') token: string,
  ): Promise<Boolean> {
    return await this.authService.verifyEmailRegistration(token);
  }

  @Mutation(returns => Boolean)
  async sendForgotPasswordEmail(
    @Args('email') email: string,
  ): Promise<boolean> {
    return await this.authService.sendForgotPasswordEmail(email);
  }

  @Mutation(returns => Boolean)
  async resetPassword(@Args('data') data: ResetPasswordDto): Promise<boolean> {
    return await this.authService.resetPassword(data);
  }
}
