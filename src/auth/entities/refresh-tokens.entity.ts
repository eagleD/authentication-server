import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { IsNotEmpty } from 'class-validator';
import { UsersEntity } from '../../users/users.entity';

@Entity('refresh_tokens')
export class RefreshTokensEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  token: string;

  @Column()
  expiresAt: Date;

  @OneToOne(type => UsersEntity, user => user.refreshToken)
  @JoinColumn()
  @IsNotEmpty()
  user: UsersEntity;
}
