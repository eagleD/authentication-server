import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';

import { UsersService } from '../users/users.service';
import { ERROR_MSG } from '../common/constants/messages';
import { JwtPayload } from './dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.ACCESS_SECRET_KEY,
      ignoreExpiration: false,
    });
  }

  async validate(payload: JwtPayload, done: VerifiedCallback) {
    const { username } = payload;
    const user = await this.userService.findOneByUsername(username);
    if (!user) {
      return done(
        new UnauthorizedException(ERROR_MSG.UNAUTHORIZED_ACCESS),
        false,
      );
    }

    return done(null, user);
  }
}
